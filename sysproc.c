#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int 
sys_reverse_number(void){
  int n;
  struct proc *curproc = myproc();
  n = curproc->tf->ebx;
	int rev = 0;
	while(n > 0){
		rev = rev * 10;
		rev = rev + n % 10;
		n = n / 10;
	}
	return rev;
}

int 
sys_trace_syscalls(void){
  int n;
  if(argint(0, &n) < 0)
    return -1;

  if(n == 1){
    set_status(1);
    cprintf("Successfully tracking the system calls.\n");
  }else if(n == 0){
    set_status(0);
    clean_map();
    cprintf("System calls untrack.\n");
  }else{
    cprintf("Invalid command trace syscall\n");
  }
  return 0;
}


int concat(int a, int b, int lenb){
  int i;
  for(i = 0; i < lenb; i++)
    a *= 10;
  return a + b;
}

int get_child_list(struct proc* procs, int n, int* count){
  int result = 0, temp, i, cnt = 0;
  for(i = 0; i < NPROC; i++){
    if((procs[i].parent)->pid == n){
      cnt = 0;
      result = concat(result, i + 1, 1);
      (*count) = 1 + (*count);
      temp = get_child_list(procs, i + 1, &cnt);
      result = concat(result, temp, cnt);
      (*count) += cnt;
    }
  }
  return result;
}

int
sys_get_children(void){
  int n;
  int count = 0;
  if(argint(0, &n) < 0)
    return -1;
  struct proc* procs;
  procs = get_all_procs();
  // cprintf("result = %d\n", get_child_list(procs, n, &count));
  // return 0;
  return get_child_list(procs, n, &count);
}

int
sys_print_trace(void){
  int i,j;
  struct proc* procs;
  procs = get_all_procs();
  char syscall_name[NSYS][20] = {"Fork" , "Exit" , "Wait" , "Pipe" , "Read" , "Kill" , "Exec","Fsat",
    "Chdir" , "Dup" , "Getpid" , "Sbrk" , "Sleep" , "Uptime" , "Open" , "Write","Mknod","Unlink","Link",
      "Mkdir" , "Close" , "Reverse_number" , "Trace_syscalls" , "Get_children" , "print_trace"};
  if(get_status() == 1){
    for(i = 0 ; i < NPROC ; i++){
        if(procs[i].pid >= 1 && procs[i].pid <= NPROC){
          cprintf("%s:\n" , procs[i].name);
          for(j = 0 ; j < NSYS ; j++)
            cprintf("\t%s:%d\n" , syscall_name[j] , get_pid_trace_number(procs[i].pid , j));
          cprintf("\n");
        }
    }
  }
  return 0;
}