#include "syscall.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(){
    int ch;
    ch = fork();
    
    if(ch == 0){
        write(1, "i am a child 1\n", 15);
        int ch2;
        ch2 = fork();
        if(ch2 == 0){
            write(1, "i am a child 2\n", 15);
            sleep(500);
            exit();
        }else{
            wait();
        }

        sleep(500);
        exit();
    }else{
        sleep(100);
        printf(1, "result = %d\n", get_children(getpid()));
        printf(1, "result = %d\n", get_children(1));
        wait();
    }
    get_children(1);
    exit();
}