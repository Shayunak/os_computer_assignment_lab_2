// init: The initial user-level program

#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char *argv[] = { "sh", 0 };

void 
printGroup(){
  printf(1, "1- soheil zibakhsh\n");
  printf(1, "2- shayan hamidi\n");
  printf(1, "3- Mahdi Hoseini\n");
}

int
main(void)
{
  int pid, wpid, trace_pid;

  if(open("console", O_RDWR) < 0){
    mknod("console", 1, 1);
    open("console", O_RDWR);
  }
  dup(0);  // stdout
  dup(0);  // stderr

  for(;;){
    printGroup();
    printf(1, "init: starting sh\n");
    printf(1, "init: starting trace\n");
    pid = fork();
    if(pid < 0){
      printf(1, "init: fork failed\n");
      exit();
    }
    if(pid == 0){
      exec("sh", argv);
      printf(1, "init: exec sh failed\n");
      exit();
    }
    trace_pid = fork();
    if(trace_pid < 0){
      printf(1, "init: fork trace failed\n");
      exit();
    }
    if(trace_pid == 0){
      exec("trace", argv);
      printf(1, "init: exec trace failed\n");
      exit();
    }
    while((wpid=wait()) >= 0 && wpid != pid)
      printf(1, "zombie!\n");
  }
}
