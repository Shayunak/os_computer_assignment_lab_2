#include "syscall.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define I(c)  ((c)-'0') //character to integer conversion

int stoi(const char *s){
  int value = 0;
  int n;
  for(n = 0; s[n]; n++)
    value = (value * 10 + I(s[n]) );

  return value;
}

int main(int argc , char* argv[]){
    if(argc != 2){
        write(1 , "Invalid arguments!\n" , 20);
        exit();
    }
    trace_syscalls(stoi(argv[1]));
    exit();
}