#include "types.h"
#include "defs.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
  if ((int)dst%4 == 0 && n%4 == 0){
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
  return dst;
}

int
memcmp(const void *v1, const void *v2, uint n)
{
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
}

void*
memmove(void *dst, const void *src, uint n)
{
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
  return memmove(dst, src, n);
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
    n--, p++, q++;
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}

char*
strncpy(char *s, const char *t, int n)
{
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
    *s++ = 0;
  return os;
}

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
  char *os;

  os = s;
  if(n <= 0)
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
    ;
  *s = 0;
  return os;
}

int
strlen(const char *s)
{
  int n;

  for(n = 0; s[n]; n++)
    ;
  return n;
}

//catenate two strings
void concatenate(char* dest_str , char* src_str){
    int i;
    int dest_str_length = strlen(dest_str);
    for(i = dest_str_length ; i < dest_str_length + strlen(src_str) ; i++){
        dest_str[i] = src_str[i - dest_str_length];
    }
}

//number of digits of an integer number
int intLenght(int n){
  int i;

  for(i = 1; (n = n / 10) ; i++);
  return i;
}

//string to integer conversion
int stoi(const char *s){
  int value = 0;
  int n;
  for(n = 0; s[n]; n++)
    value = (value * 10 + I(s[n]) );

  return value;
}

//integer to string conversion
void itos(char* output , int input){
    int i,d;
    int n = intLenght(input);
    for(i = 1 ; i <= n ; i++){
        d = input % 10;
        input = (input - d) / 10;
        output[n-i] = S(d);
    }
}