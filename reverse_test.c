#include "syscall.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define I(c)  ((c)-'0') //character to integer conversion
int stoi(const char *s){
  int value = 0;
  int n;
  for(n = 0; s[n]; n++)
    value = (value * 10 + I(s[n]) );

  return value;
}

int main(int argc, char *argv[]){
    if(argc < 2){
		printf(1 , "reverse number: Not Enough arguments.\n");
		exit();
	}
	if(argc > 2){
		printf(1 , "reverse number: Too many arguments.At most %d arguments is supported.\n" , 2);
		exit();
	}

    printf(1, "%d\n", reverse_number(stoi(argv[1])));

    exit();
}